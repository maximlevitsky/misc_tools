#! /bin/bash

BASE=$(dirname "$BASH_SOURCE")
BASE=$(realpath $BASE)

TTY=ttyS0

if [[ $UID != 0 ]] ; then
    exec sudo $(realpath $0) "$@"
fi


#########################################################################

cat << EOF > /usr/local/bin/serial-shell-${SUDO_USER}.sh
#! /bin/bash

export PROMPT_COMMAND=/usr/local/bin/tty_resize
export TERM=xterm-256color

cd /home/${SUDO_USER}
exec su  ${SUDO_USER} -l -w PROMPT_COMMAND,TERM
EOF

chmod +x /usr/local/bin/serial-shell-${SUDO_USER}.sh

#########################################################################

cat << EOF > /etc/systemd/system/simple-serial-${SUDO_USER}@.service
[Unit]
Description=Simple passwordless serial Getty on %I
BindsTo=dev-%i.device
After=dev-%i.device systemd-user-sessions.service getty-pre.target

Before=getty.target
IgnoreOnIsolate=yes
Conflicts=rescue.service
Before=rescue.service

[Service]
ExecStart=-/sbin/agetty -n --keep-baud 115200,38400,9600 --noclear --login-program /usr/local/bin/serial-shell-${SUDO_USER}.sh  %I $TERM

Type=idle
Restart=always
UtmpIdentifier=%I
TTYPath=/dev/%I
TTYReset=yes
TTYVHangup=yes
KillMode=process
IgnoreSIGPIPE=no
SendSIGHUP=yes

[Install]
WantedBy=getty.target

EOF

#########################################################################

cp $BASE/tty_resize /usr/local/bin
systemctl daemon-reload

systemctl mask serial-getty@ttyS0.service
systemctl stop --now serial-getty@$TTY.service
systemctl enable --now simple-serial-${SUDO_USER}@$TTY.service
