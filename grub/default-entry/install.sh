#! /bin/bash

script_dir=$(dirname "$BASH_SOURCE")
script_dir=$(realpath $script_dir)

sudo cp $script_dir/set-grub-boot-entry.service /etc/systemd/system/
sudo cp $script_dir/set-grub-boot-entry /usr/local/bin

sudo systemctl daemon-reload
sudo systemctl enable set-grub-boot-entry