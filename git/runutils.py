
import subprocess
import re

def run(command, cwd = None, quiet = True, check = True):
	process = subprocess.Popen(command, shell = True, stdout = subprocess.PIPE, stderr = subprocess.PIPE, cwd = cwd)

	output = process.communicate()
	out_stdout = output[0].decode('utf-8').strip()
	out_stderr = output[1].decode('utf-8').strip()

	if process.returncode != 0:
		if check:
			print(command + " FAILED")
			print(out_stderr)
			exit(1)

	if (not quiet) and out_stderr != "":
		print(out_stderr)

	return out_stdout
