#!/usr/bin/env python3

import argparse
import os
import subprocess
import sys

##################################################################################################################

def write_file(path, value):
    with open(path, "w") as file:
        file.write(value)

class UserError(Exception):
    pass


##################################################################################################################
args = argparse.ArgumentParser()
args.add_argument("--device", help="Device to attach/detach")
args.add_argument("--domain", help="Domain to attach/detach", type=int)
args.add_argument("--driver", help="Advance: Which driver to bind to device", choices=["vfio_pci", "uio_pci"], default="vfio_pci")
args.add_argument("--attach", "--bind",   help="attach the device to VFIO_PCI (and remove its device driver)", action="store_true")
args.add_argument("--detach", "--unbind", help="detach the device from VFIO_PCI (and attach to its device driver)", action="store_true")
    
args = args.parse_args()

if args.driver == "vfio_pci":
    driver = "vfio-pci"
else:
    driver = "uio_pci_generic"
    
##################################################################################################################
if os.getuid() != 0:
    os.execv("/bin/sudo", ["--"] + sys.argv)
    assert(0)

##################################################################################################################

class Device:
    def __init__(self,domainid, devicepath):
        
        self.domainid = domainid
        self.printable_attrs = ["path","driver","iommu"]
        self.path = devicepath
        self.name = devicepath.split("/")[-1]
        
        self.driver = None
        if (os.path.exists(devicepath + "/driver")):
            self.driver = os.path.realpath(devicepath + "/driver").split("/")[-1]

        self.iommu = None
        if (os.path.exists(devicepath + "/iommu")):
            self.iommu = os.path.realpath(devicepath + "/iommu").split("/")[-1]

    def get_name(self):
        return self.name
    
    def is_vfio_bound(self):
        return self.driver == "vfio-pci" or self.driver == "uio_pci_generic"

    def vfio_bind(self):
        
        if self.is_vfio_bound():
            print("Device is already VFIO/UIO bound")
            return
        
        subprocess.run(["modprobe", driver], check=True) # for in case when it is not loaded
        
        print("Attaching device " + self.name + " to VFIO/UIO")        

        if self.driver != None:
            write_file(self.path + "/driver/unbind", self.name)            

        write_file(self.path +"/driver_override", driver)
        write_file(self.path +"/subsystem/drivers_probe", self.name)

    def vfio_unbind(self):
        if not self.is_vfio_bound():
            print("Device is not VFIO bound")
            return

        print("Detaching device " + self.name + " from VFIO/UIO")
        write_file(self.path + "/driver/unbind",  self.name)            
        write_file(self.path +"/driver_override", "\n")
        write_file(self.path +"/subsystem/drivers_probe", self.name)
        
        
    def vfio_bind_unbind(self, is_bind):
        self.vfio_bind() if is_bind else self.vfio_unbind()

    def print_me(self):
        print(" * " + self.name + ":")
        for name in self.printable_attrs:
            if hasattr(self,name) and getattr(self, name) != None:
                print("   " + name + ": " + getattr(self, name))

##################################################################################################################

# read all IOMMU domains
iommu_domains = {}
all_devices = {}

basepath = "/sys/kernel/iommu_groups"
for domainid in os.listdir(basepath):
    domain_devices = {}
    devicespath = basepath + "/" + domainid + "/devices"
    
    for devicelink in  os.listdir(devicespath):            
        device = Device(domainid, os.path.realpath(devicespath + "/" + devicelink))
        domain_devices[device.get_name()] = device
        all_devices[device.get_name()] = device

    iommu_domains[int(domainid)] = domain_devices

##################################################################################################################

if args.device != None and args.device not in all_devices:
    print ("Device " + args.device + " either doesn't exit or has no IOMMU")

if args.domain != None and args.domain not in iommu_domains:
    print ("Domain " + str(args.domain) + " doesn't exist")

##################################################################################################################
if not args.attach and not args.detach:

    if args.device:
        all_devices[args.device].print_me()
    elif args.domain != None:
        for devicename, device in sorted(iommu_domains[args.domain].items()):
            device.print_me()
            print("")
    else:
        for (domainid, domain_devices) in sorted(iommu_domains.items()):
            print ( "Domain " + str(domainid))
            for devicename, device in sorted(domain_devices.items()):
                device.print_me()
                print("")

##################################################################################################################
else:
    if args.device:
        all_devices[args.device].vfio_bind_unbind(args.attach)
    
    elif args.domain != None:
        for device in iommu_domains[args.domain].values():
            device.vfio_bind_unbind(args.attach)

    elif args.detach:        
        for device in all_devices.values():
            if device.is_vfio_bound():
                device.vfio_unbind()
    else:
        print ("ERROR: refusing to attach all the PCI devices to the VFIO")
