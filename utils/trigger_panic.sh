#! /bin/bash

if [[ $UID != 0 ]] ; then
    exec sudo $(realpath $0) "$@"
fi

echo 5 > /proc/sys/kernel/panic

echo "c" > /proc/sysrq-trigger
