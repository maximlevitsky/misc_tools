#! /bin/bash

export UB_RESULTDIR=$PWD/$1/

for iteration in {1..50} ; do

        rm -rf /tmp/byte-unixbench

        git clone --depth=1 https://github.com/kdlucas/byte-unixbench.git /tmp/byte-unixbench

        pushd /tmp/byte-unixbench/UnixBench
	export UB_OUTPUT_FILE_NAME=result_$iteration
        ./Run -c $(nproc)
        popd
done
