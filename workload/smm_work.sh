#! /bin/bash

DIR=$(dirname "$0")


DELAY=200
COUNT=0

while [[ $# > 0 ]] ; do
  case "$1" in
    --tco)
      TRIGGER=tco
      ;;
    --std)
      TRIGGER=pm
      ;;
    --hard)
      DELAY=0
      ;;
    --soft)
      DELAY=10000
      ;;
    --once)
      COUNT=1
      ;;
  esac
  shift
done

core_count=$(nproc --all)
trigger_core=$((core_count-1))

if [[ $TRIGGER == "" ]] ; then
    if [ -d /sys/firmware/efi ] ; then
        #TODO: very old OVMF and Qemu might still use unicast #SMI - need to check fw_config
        echo "UEFI BIOS detected, triggering broadcast #SMI via io port 0xB2 write, from cpu $trigger_core"
        TRIGGER=pm
    else
        echo "PC BIOS detected, triggering unicast #SMI on CPU0 via io port 0x662 write, from cpu $trigger_core"
        TRIGGER=tco
    fi
fi

if [ -e /sys/module/kvm_amd/parameters/intercept_smi ] ; then
    echo "KVM_AMD intercept_smi=$(cat /sys/module/kvm_amd/parameters/intercept_smi)"
fi


case $TRIGGER in
    pm)
        sudo taskset -c $trigger_core $DIR/outb_flood 0xb2 0xAA $DELAY $COUNT
        ;;
    tco)
        sudo taskset -c $trigger_core $DIR/outb_flood 0x662 0xBB $DELAY $COUNT
        ;;
esac
