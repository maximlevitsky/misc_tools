#!/usr/bin/env python3

import re
import os
import argparse

def avg_list(list1):

    if list1 == None or len(list1) == 0:
        return 0

    sum = sum_list(list1)
    return sum / len(list1)

def sum_list(list1):
    if list1 == None:
        return 0
    sum = 0
    for e in list1:
        if e != None:
            sum += e
    return sum


########################################################################3
def parse_single_file(results, file):

    skip_to_results = True

    for line in file:

        if skip_to_results:
            if line.startswith("System Benchmarks Index Values"):
                skip_to_results = False
                continue
        else:

            if "========" in line:
                continue

            name = line.split("  ")[0]

            name = name.replace(" ", "_")
            name = name.replace("(", "")
            name = name.replace(")", "")
            name = name.lower()

            try:
                value = float(line.split()[-1])
            except IndexError:
                continue

            if name not in results:
                results[name] = []

            results[name].append(value)

########################################################################3



parser = argparse.ArgumentParser(add_help = False)
parser.add_argument("dir", default = os.getcwd())

args = parser.parse_args()



results = {}

for filename in os.listdir(args.dir):
    if '.' in filename:
        continue

    with open(args.dir + "/" + filename, "r") as file:
        parse_single_file(results, file)


for key in results.keys():
    print(key, end = "\t:\t")
    print(str(avg_list(results[key])))
