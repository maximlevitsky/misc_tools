#! /bin/bash

if [ -e /dev/vda ] ; then
	TEST_FILE=/dev/vda
elif [ -e /dev/sda ] ; then
	TEST_FILE=/dev/sda
else
	echo "error: VM doesn't have standard disk!"
fi

sudo fio --numjobs=1 --name=job --runtime=6000000000 --time_based \
         --filename=$TEST_FILE \
         --ioengine=libaio --direct=1 --rw=randread --bs=4K --cpus_allowed_policy=split \
         --thread  --clocksource=cpu --group_reporting \
         --iodepth=128 \
         --iodepth_batch_submit=8 --iodepth_batch_complete_min=1 --iodepth_batch_complete_max=16
