#! /bin/bash

script_dir=$(dirname "$BASH_SOURCE")
script_dir=$(realpath $script_dir)

export PATH=$script_dir/grub/:$script_dir/qemu/:$script_dir/systemd:$PATH
export PATH=$script_dir/utils:$script_dir/workload:$script_dir/info/:$PATH
export PATH=$script_dir/git/:$PATH

source $script_dir/grub/_completion
source $script_dir/qemu/_completion
source $script_dir/systemd/_completion
